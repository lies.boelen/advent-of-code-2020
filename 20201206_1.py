import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    alphabet_list = list(string.ascii_lowercase)
    letter_to_index = {k: v for v, k in enumerate(alphabet_list)}
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        grp = 0
        out = {}
        out[grp] = [0] * 26
        while line:
            if line == "\n":
                grp += 1
                out[grp] = [0] * 26
            else:
                for letter in line.replace("\n", ""):
                    out[grp][letter_to_index[letter]] += 1
            line = fp.readline()
    out_array = np.array(list(out.values()))
    print("out_array @{}".format(out_array.shape))
    return out_array


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    yes_per_group = np.minimum(dat, 1).sum(axis=1)
    print("Total sum of yes per group = {}".format(yes_per_group.sum()))


if __name__ == "__main__":
    main()