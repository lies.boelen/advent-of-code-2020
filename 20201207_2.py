import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    out = {}
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        while line != "":
            linesplit = line.split(" bags contain ")
            outer_colour = linesplit[0]
            out[outer_colour] = {}
            inside = linesplit[1]
            if inside == "no other bags.\n":
                pass
            else:
                inside = (
                    inside.replace(" bags", "").replace(" bag", "").replace(".\n", "")
                )
                inside_split = inside.split(", ")
                for k in inside_split:
                    num, col1, col2 = k.split(" ")
                    out[outer_colour]["{} {}".format(col1, col2)] = int(num)
            line = fp.readline()
    key_colours = list(out.keys())
    inside_colours = [list(v.keys()) for v in out.values()]
    all_colours = key_colours + [
        colour for sublist in inside_colours for colour in sublist
    ]
    all_colours_unique = pd.Series(all_colours).unique()  # np array
    container_df = pd.DataFrame(0, index=all_colours_unique, columns=all_colours_unique)
    for k, v in out.items():
        if len(v) > 0:
            for k1, v1 in v.items():
                container_df.loc[k, k1] = v1

    return container_df


def count_bags_inside(colour, container_df):
    count = 0
    vals = container_df.loc[colour].values
    if vals.sum() > 0:
        for v, col in zip(vals[vals > 0], container_df.columns.values[vals > 0]):
            count += v + v * count_bags_inside(col, container_df)
    return count


@click.command()
@click.argument("fn")
@click.option("--calc-df", default=False)
def main(fn, calc_df):
    container_df_filename = "20201207_container_df.csv"
    if calc_df:
        container_df = read_data(fn)
        container_df.to_csv(container_df_filename)
    else:
        container_df = pd.read_csv(container_df_filename)
    column_colours = container_df.columns.values  # np array
    inside_count = count_bags_inside("shiny gold", container_df)
    print("Bag inside my shiny gold bag = {}".format(inside_count))


if __name__ == "__main__":
    main()