import numpy as np
import pandas as pd

import itertools

import click

# import re
# import sys


PLAYER1 = [
    21,
    48,
    44,
    31,
    29,
    5,
    23,
    11,
    12,
    27,
    49,
    22,
    18,
    7,
    15,
    20,
    2,
    45,
    14,
    17,
    40,
    35,
    6,
    24,
    41,
]

PLAYER2 = [
    47,
    1,
    10,
    16,
    28,
    37,
    8,
    26,
    46,
    25,
    3,
    9,
    34,
    50,
    32,
    36,
    43,
    4,
    42,
    33,
    19,
    13,
    38,
    39,
    30,
]

# # Example given
# PLAYER1 = [9, 2, 6, 3, 1]
# PLAYER2 = [5, 8, 4, 7, 10]

# # Example of game that needs infinity rule
# PLAYER1 = [43, 19]
# PLAYER2 = [2, 29, 14]


def play_round(player1, player2, prev_rounds_in_game, round_num, game_num):
    prefix = "".join([" "] * (game_num * 2))
    # check whether the same deck has been played already
    if len(prev_rounds_in_game) > 0:
        print
        for prev in prev_rounds_in_game:
            prev1, prev2 = prev
            if (player1 == prev1) * (player2 == prev2):
                print(
                    "{}Player 1 wins round {} of game {} (by infinity rule)".format(
                        prefix, round_num, game_num
                    )
                )
                # player1 wins the game immediately
                return [0], []
    # draw cards
    p1 = player1.pop(0)
    p2 = player2.pop(0)
    print("{}cards played = {} - {}".format(prefix, p1, p2))
    # compare to remaining cards
    cond1 = p1 <= len(player1)
    cond2 = p2 <= len(player2)
    if cond1 & cond2:
        subgame1_deck, subgame2_deck = play_game(
            player1[:p1].copy(), player2[:p2].copy(), game_num + 1
        )
        # TODO what is the output of this??
        if len(subgame1_deck) > 0:
            print(
                "{}Player 1 wins round {} of game {} (by subgame)".format(
                    prefix, round_num, game_num
                )
            )
            player1 += [p1, p2]
        else:
            print(
                "{}Player 2 wins round {} of game {} (by subgame)".format(
                    prefix, round_num, game_num
                )
            )
            player2 += [p2, p1]
    else:
        # finish the round
        if p1 > p2:
            print(
                "{}Player 1 wins round {} of game {} (by highest card)".format(
                    prefix, round_num, game_num
                )
            )
            player1 += [p1, p2]
        else:
            print(
                "{}Player 2 wins round {} of game {} (by highest card)".format(
                    prefix, round_num, game_num
                )
            )
            player2 += [p2, p1]
    return player1, player2


def play_game(player1, player2, game_num=1):
    print("+++ Starting game {} +++".format(game_num))
    previous_rounds_in_game = []
    round_num = 1
    while len(player1) * len(player2) > 0:
        current_round = [player1.copy(), player2.copy()]
        player1, player2 = play_round(
            player1, player2, previous_rounds_in_game, round_num, game_num
        )
        previous_rounds_in_game += [current_round]
        round_num += 1
        if player1 == [0]:
            # game was won by player one
            return previous_rounds_in_game[-1]
    # winner is the one who still has cards
    winner_num = 1 if player2 == [] else 2
    print("Player {} wins game {}".format(winner_num, game_num))
    print("+++ Ending game {} +++".format(game_num))
    return player1, player2


def calculate_score(arr):
    return (np.array(arr) * (1 + np.arange(len(arr)))[::-1]).sum()


def main():
    deck1, deck2 = play_game(PLAYER1, PLAYER2)
    print(deck1, deck2)
    answer = calculate_score(deck1 if len(deck1) > 0 else deck2)
    print("Answer = {}".format(answer))


if __name__ == "__main__":
    main()