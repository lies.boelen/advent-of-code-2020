import numpy as np
import pandas as pd
import itertools

import click

import string


def read_data(fn):
    # return 2D np array with 0/1 (np.int8)
    with open(fn, "r") as fp:
        input_list = []
        line = fp.readline().replace("\n", "")
        while line:
            input_list += [[np.int8(1) if x == "#" else np.int8(0) for x in line]]
            line = fp.readline().replace("\n", "")

    out = np.array(input_list)
    return out


def count_active_neighbours(game_array):
    counter = np.zeros_like(game_array)
    dx, dy, dz = counter.shape
    opt_list = [-1, 0, 1]
    for a, b, c in itertools.product(opt_list, opt_list, opt_list):
        # direction is an array of length 3 with entries 0/1/-1; 26 such directions
        # as we are not including (0, 0, 0)
        if np.abs([a, b, c]).sum() > 0:
            counter[
                np.maximum(0, 0 + a) : np.minimum(dx, dx + a),
                np.maximum(0, 0 + b) : np.minimum(dy, dy + b),
                np.maximum(0, 0 + c) : np.minimum(dz, dz + c),
            ] += game_array[
                np.maximum(0, 0 - a) : np.minimum(dx, dx - a),
                np.maximum(0, 0 - b) : np.minimum(dy, dy - b),
                np.maximum(0, 0 - c) : np.minimum(dz, dz - c),
            ]
    return counter


def update_game_array(game_array):
    neighbour_count = count_active_neighbours(game_array)
    # update active cubes: exactly 2 or 3 active neighbours, otherwise become inactive
    cond_become_inactive = (game_array == 1) & (
        (neighbour_count < 2) | (neighbour_count > 3)
    )
    # update inactive cubes: have exactly 3 active neighbours
    cond_become_active = (game_array == 0) & (neighbour_count == 3)
    game_array[cond_become_inactive] = 0
    game_array[cond_become_active] = 1
    return game_array


@click.command()
@click.argument("fn")
def main(fn):
    input_array = read_data(fn)
    dx, dy = input_array.shape
    # need to expand this to 3d
    game_array = np.zeros((dx + 2 * 6, dy + 2 * 6, 1 + 2 * 6), dtype=np.int8)
    game_array[6 : 6 + dx, 6 : 6 + dy, 6] = input_array
    # for six rounds, update this according to the game rules
    for game_round in range(6):
        game_array = update_game_array(game_array)
    print("Number of active cells = {}".format(game_array.sum()))


if __name__ == "__main__":
    main()
