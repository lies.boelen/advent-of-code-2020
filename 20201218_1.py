import numpy as np
import pandas as pd
import itertools

import click

import re


def read_data(fn):
    # return list of strings
    with open(fn, "r") as fp:
        input_list = []
        line = fp.readline().replace("\n", "").replace(" ", "")
        while line:
            input_list += [line]
            line = fp.readline().replace("\n", "").replace(" ", "")
    return input_list


def solve_one_line(string):
    """ solve in order of appearance, overridden by parentheses """
    # first look for parentheses
    if "(" in string:
        op_split = string.split("(", 1)
        op_side_ll = op_split[0]
        # need to find the MATCHING parenthesis
        lpar_count = 0
        for i, e in enumerate(op_split[1]):
            if (e == ")") & (lpar_count == 0):
                split_at = i
                break
            elif e == ")":
                lpar_count -= 1
            elif e == "(":
                lpar_count += 1
        inside_parentheses, op_side_rr = (
            op_split[1][:split_at],
            op_split[1][split_at + 1 :],
        )
        return solve_one_line(
            "{}{}{}".format(op_side_ll, solve_one_line(inside_parentheses), op_side_rr)
        )
    # if there are no parentheses left, just go left to right
    else:
        numbers = [int(x) for x in re.split("[+*]", string)]
        ops = re.sub("[0-9]", "", string)
        result = numbers.pop(0)
        for op in ops:
            if op == "+":
                result += numbers.pop(0)
            else:
                result *= numbers.pop(0)
        return result


@click.command()
@click.argument("fn")
def main(fn):
    input_list = read_data(fn)
    size_homework = len(input_list)
    results = np.zeros(size_homework)
    for i in range(len(input_list)):
        print(input_list[i])
        results[i] = solve_one_line(input_list[i])
        print(results[i])
        print("--------")
    print("Sum of homework = {}".format(results.sum()))


if __name__ == "__main__":
    main()
