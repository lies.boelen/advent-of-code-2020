import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return
    # 1. number of minutes before you can start boarding
    # 2. all bus IDs currently running
    with open(fn, "r") as fp:
        # read into a dict
        fp.readline()
        bus_ids = fp.readline().replace("\n", "")
    bus_ids_split = bus_ids.split(",")
    bus_ids_running = np.array([int(x) if x != "x" else np.nan for x in bus_ids_split])
    return bus_ids_running


def find_mod_inv(x, y):
    for i in range(1, y):
        if np.mod(x * i, y) == 1:
            return i


def is_prime(x):
    for i in range(2, x):
        if np.mod(x, i) == 0:
            return False
    return True


@click.command()
@click.argument("fn")
def main(fn):
    bus_ids_running = read_data(fn)
    t_plus_what = -np.arange(len(bus_ids_running))
    mask = np.isfinite(bus_ids_running)
    divs = bus_ids_running[mask].astype(np.uint64)
    remainders = t_plus_what[mask]
    remainders = np.mod(remainders, divs).astype(np.uint64)
    # chinese remainder theorem: solve with Gauss' algorithm
    N = divs.prod()
    Nis = (N / divs).astype(np.uint64)
    Ni_inverses = np.array([find_mod_inv(x, y) for x, y in zip(Nis, divs)])
    t2 = np.uint64(0)
    for rem, ni, ni_inv, div in zip(remainders, Nis, Ni_inverses, divs):
        adding = np.mod(ni * ni_inv, N).astype(np.uint64)
        adding = np.mod(adding * rem, N).astype(np.uint64)
        t2 += adding
        t2 = np.mod(t2, N).astype(np.uint64)
        assert np.mod(t2, div) == rem, "numerical instability?"
    print(int(t2))


if __name__ == "__main__":
    main()
