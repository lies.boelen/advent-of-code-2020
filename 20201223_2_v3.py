import pandas as pd
import numpy as np

import numba


def play_one_round(game_status, current_val):
    N = len(game_status)
    # find what will come next to the current value
    a = current_val
    take_out = [0, 0, 0]
    for i in range(3):
        a = game_status[a]
        take_out[i] = a
    updated_pointer_from_current = game_status[a]
    # find destination
    destination_val = current_val - 1
    while (destination_val in take_out) or (destination_val == 0):
        # print("no can do {}".format(destination_val))
        destination_val = destination_val - 1 if destination_val > 1 else N
    # find what last element of take_out will point to - get from destination
    updated_pointer_from_last_taken_out = game_status[destination_val]

    game_status[current_val] = updated_pointer_from_current
    game_status[destination_val] = take_out[0]
    game_status[take_out[-1]] = updated_pointer_from_last_taken_out

    return game_status, updated_pointer_from_current


def print_game_status(game_status, current_val):
    out_str = "({})".format(current_val)
    new_val = game_status[current_val]
    while new_val != current_val:
        out_str += " {} ".format(new_val)
        new_val = game_status[new_val]
    print(out_str)


def generate_ll(input_list):
    out_dict = {l: n for l, n in zip(input_list[:-1], input_list[1:])}
    out_dict[input_list[-1]] = input_list[0]
    return out_dict


def main():
    input_str = "586439172"
    # input_str = "389125467"  # online example
    game_status = np.concatenate(
        [
            np.array([np.int(x) for x in input_str]),
            np.arange(len(input_str), 1_000_000, dtype=np.int) + 1,
        ]
    )
    # game_status = np.array([int(x) for x in input_str])  # part 1
    current_val = game_status[0]  # get current value before turning this into a list
    game_status = generate_ll(game_status)  # this is a dict
    for i in range(10_000_000):
        if np.mod(i, 1000) == 0:
            print("+++ round {} +++".format(i))
        # print_game_status(game_status, current_val)
        game_status, current_val = play_one_round(game_status, current_val)
    print("+++ end of game +++")
    # print_game_status(game_status, current_val)
    # labels after cup 1
    a1 = game_status[1]
    a2 = game_status[a1]
    answer = a1 * a2
    print("Answer: {} * {} = {}".format(a1, a2, answer))


if __name__ == "__main__":
    main()