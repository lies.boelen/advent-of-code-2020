import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return
    # 1. rules
    # 2. array your ticket
    # 3. array: other tickets
    with open(fn, "r") as fp:
        line = fp.readline().replace("\n", "")
        setting = "rules"
        rule_dict = {}
        nearby_ticks_list = []
        while line:
            line = line.replace("\n", "")
            if setting == "rules":
                if line != "":
                    a, b = line.split(": ")
                    c, d = b.split(" or ")
                    c1, c2 = c.split("-")
                    d1, d2 = d.split("-")
                    rule_dict[a] = [[int(c1), int(c2)], [int(d1), int(d2)]]
                else:
                    setting = "my_ticket"

            elif setting == "my_ticket":
                if (line != "") & (line != "your ticket:"):
                    my_tick_array = np.array(
                        [int(x) for x in line.split(",")], dtype=int
                    )
                if line == "":
                    setting = "nearby_tickets"

            elif setting == "nearby_tickets":
                if (line != "") & (line != "nearby tickets:"):
                    nearby_ticks_list += [line.split(",")]

            line = fp.readline()

    nearby_ticks_array = np.array(nearby_ticks_list, dtype=int)
    return rule_dict, my_tick_array, nearby_ticks_array


def process_rule_dict_to_array(rule_dict):
    """
    return an array of numbers that are allowed on tickets (over all fields)
    """
    # first, find the max of all rules
    max_val_allowed = 0
    for v in rule_dict.values():
        if v[0][-1] > max_val_allowed:
            max_val_allowed = v[0][-1]
        if v[1][-1] > max_val_allowed:
            max_val_allowed = v[1][-1]
    allowed_arr = np.zeros(max_val_allowed + 1, dtype=np.int8)
    for v in rule_dict.values():
        allowed_arr[v[0][0] : (v[0][1] + 1)] = 1
        allowed_arr[v[1][0] : (v[1][1] + 1)] = 1
    return np.where(allowed_arr == 1)[0]


def process_nearby_tickets(nearby_tickets_array):
    """
    return a matrix with 0/1 for numbers on the tickets
    """
    d2 = np.max(nearby_tickets_array) + 1
    out = np.zeros((nearby_tickets_array.shape[0], d2), dtype=np.int8)
    for i in range(out.shape[0]):
        out[i, nearby_tickets_array[i]] = 1
    assert (out.sum(axis=-1) <= nearby_tickets_array.shape[1]).all()
    return out


@click.command()
@click.argument("fn")
def main(fn):
    rule_dict, my_tick_array, nearby_ticks_array = read_data(fn)
    rule_array = process_rule_dict_to_array(rule_dict)
    nearby_ticket_bin_mat = process_nearby_tickets(nearby_ticks_array)
    a = nearby_ticket_bin_mat * np.arange(nearby_ticket_bin_mat.shape[1])[None, :]
    error_rate = a[:, [x for x in range(a.shape[-1]) if x not in rule_array]].sum()
    print("Error rate = {}".format(error_rate))


if __name__ == "__main__":
    main()
