import numpy as np
import pandas as pd

# import itertools

import click

# import re
# import sys
# sys.setrecursionlimit(1500)


def read_data(fn):
    # return an array of tiles (144, 10, 10)
    tile_number_list = []
    tile_list = []
    tile = []
    # tilemode = False
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            if line == "\n":
                if tile != []:
                    tile_list += [tile]
            elif line[:4] == "Tile":
                tile_number_list += [int(line.replace("Tile ", "").replace(":", ""))]
                tile = []
            else:
                # add to tile
                tile += [[1 if x == "#" else 0 for x in line.replace("\n", "")]]
            line = fp.readline()
    assert len(tile_number_list) == len(tile_list), (
        len(tile_number_list),
        len(tile_list),
    )
    return np.array(tile_number_list), np.array(tile_list)


def get_side(tile, s):
    if s == 0:
        return tile[0]
    elif s == 1:
        return tile[:, -1]
    elif s == 2:
        return tile[-1]
    elif s == 3:
        return tile[:, 0]
    else:
        print("wrong side argument {}".format(s))


def create_match_matrix(tile_array):
    n_tile, _, _ = tile_array.shape
    out = np.zeros((n_tile, 4, n_tile, 4))
    out_rev = np.zeros((n_tile, 4, n_tile, 4))
    for ti in range(n_tile):
        tile1 = tile_array[ti]
        for si in range(4):
            side1 = get_side(tile1, si)
            for tii in range(n_tile):
                tile2 = tile_array[tii]
                if ti != tii:
                    for sii in range(4):
                        side2 = get_side(tile2, sii)
                        if (side1 == side2).all():
                            out[ti, si, tii, sii] = 1
                        elif (side1 == side2[::-1]).all():
                            out_rev[ti, si, tii, sii] = 1
    return out, out_rev


def find_number_matching_sides(A, A_rev):
    return ((A + A_rev).sum(axis=(2, 3)) > 0).sum(axis=-1)  # (tile,)


@click.command()
@click.argument("fn")
def main(fn):
    tile_number_array, tile_array = read_data(fn)
    print(tile_number_array.shape)
    print(tile_array.shape)
    # create matrix which counts the number of possible matching sides
    A, A_rev = create_match_matrix(tile_array)
    print(A.shape)
    number_matching_sides = find_number_matching_sides(A, A_rev)
    print(number_matching_sides)
    if (number_matching_sides == 2).sum() == 4:
        print(
            "Answer = {}".format(tile_number_array[number_matching_sides == 2].prod())
        )


if __name__ == "__main__":
    main()