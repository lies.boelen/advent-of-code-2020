import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return as numpy array
    return np.sort(pd.read_csv(fn, header=None).values.flatten())


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    dat = np.concatenate([np.array([0]), read_data(fn), np.array([dat[-1] + 3])])
    diffs = dat[1:] - dat[:-1]
    bc = np.bincount(diffs.astype(int))
    answer = bc[1] * bc[3]
    print("(1 jolt diffs) * (3 jolt diffs) = {}".format(answer))


if __name__ == "__main__":
    main()