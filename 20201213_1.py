import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return
    # 1. number of minutes before you can start boarding
    # 2. all bus IDs currently running
    with open(fn, "r") as fp:
        # read into a dict
        num_min = int(fp.readline().replace("\n", ""))
        bus_ids = fp.readline().replace("\n", "")
    bus_ids_split = bus_ids.split(",")
    bus_ids_running = np.array([int(x) for x in bus_ids_split if x != "x"])
    return num_min, bus_ids_running


@click.command()
@click.argument("fn")
def main(fn):
    num_min, bus_ids_running = read_data(fn)
    # find bus closest to but after num_min
    bustimes_after_I_arrive = (
        np.ceil(num_min / bus_ids_running) * bus_ids_running
    ).astype(int)
    my_bus_id = bus_ids_running[np.argmin(bustimes_after_I_arrive)]
    waiting_time = np.min(bustimes_after_I_arrive) - num_min
    # return bus_id * wait time
    print("Answer = {}".format(my_bus_id * waiting_time))


if __name__ == "__main__":
    main()