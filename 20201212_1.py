import numpy as np
import pandas as pd

import click


direction_dict = {
    "N": np.array([0, 1]),
    "E": np.array([1, 0]),
    "S": np.array([0, -1]),
    "W": np.array([-1, 0]),
}

R_string = "NESW"
L_string = "ENWS"


def read_data(fn):
    # return as a list
    return pd.read_csv(fn, header=None).values.flatten().tolist()


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # instructions as a list

    position = np.array([0, 0])
    facing = "E"

    for d in dat:
        print(d)
        movement = d[0]
        size = int(d[1:])
        if movement in direction_dict.keys():
            position += direction_dict[movement] * size
        elif movement == "F":
            position += direction_dict[facing] * size
        elif movement == "R":
            curr_facing_idx = list(R_string).index(facing)
            num_turns = int(size / 90)
            new_facing_idx = np.mod(curr_facing_idx + num_turns, len(R_string))
            facing = R_string[new_facing_idx]
        else:  # movement == "L"
            curr_facing_idx = list(L_string).index(facing)
            num_turns = int(size / 90)
            new_facing_idx = np.mod(curr_facing_idx + num_turns, len(L_string))
            facing = L_string[new_facing_idx]

    print("Manhattan distance travelled = {}".format(np.abs(position).sum()))


if __name__ == "__main__":
    main()