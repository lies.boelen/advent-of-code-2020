import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return as numpy array
    return np.sort(pd.read_csv(fn, header=None).values.flatten())


def find_number_of_combos(dat):
    """ dat is ordered large > small """
    mem = {}
    for i, el in enumerate(dat):
        if i == 0:
            mem[el] = 1
        else:
            values_allowed = dat[(dat > el) & (dat < el + 4)]
            mem[el] = sum([mem[k] for k in values_allowed])
    return mem.get(1, 0) + mem.get(2, 0) + mem.get(3, 0)


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    answer = find_number_of_combos(dat[::-1])
    print("number of combos = {}".format(answer))


if __name__ == "__main__":
    main()