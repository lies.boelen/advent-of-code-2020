import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return as numpy array
    return pd.read_csv(fn, header=None).values.flatten()


def check_sum(arr, el):
    """
    check whether el can be found as the sum of two values in arr
    """
    print("goal = {}".format(el))
    for i, el1 in enumerate(arr[:-1]):
        if el - el1 in arr[(i + 1) :]:
            return True
    return False


def find_first_wrong(arr, len_before=25):
    i = len_before
    while True:
        checksum = check_sum(arr[(i - len_before) : i], arr[i])
        if not checksum:
            print(arr[i], arr[(i - len_before) : i])
            return arr[i]
        i += 1


def find_encryption_weakness(dat, val):
    # find contiguous subset of dat that adds up to val
    found = False
    contig_set_len = 2
    contig_set_sum = dat[:-1] + dat[1:]
    if val in contig_set_sum:
        found = True
        found_idx = np.where(contig_set_sum == val)[0][0]
        print("found idx = {}".format(found_idx))
        contig_subset = dat[found_idx : (found_idx + contig_set_len)]
    while not found:
        contig_set_len += 1
        contig_set_sum = contig_set_sum[:-1] + dat[(contig_set_len - 1) :]
        if val in contig_set_sum:
            found = True
            found_idx = np.where(contig_set_sum == val)[0][0]
            print("found idx = {}".format(found_idx))
            contig_subset = dat[found_idx : (found_idx + contig_set_len)]
    print("found idx = {}".format(found_idx))
    print("contig set len = {}".format(contig_set_len))
    print("contig_subset = {}".format(contig_subset))
    # encryption weakness = sum of min and max of that set
    return contig_subset.max() + contig_subset.min()


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    first_wrong = find_first_wrong(dat, len_before=25)
    ew = find_encryption_weakness(dat, first_wrong)
    print("encryption weakness = {}".format(ew))


if __name__ == "__main__":
    main()