import numpy as np
import pandas as pd

import itertools

import click

# import re
# import sys
# sys.setrecursionlimit(1500)

DIR_DICT = {
    "ne": np.array([1, 1]),
    "e": np.array([2, 0]),
    "se": np.array([1, -1]),
    "sw": np.array([-1, -1]),
    "w": np.array([-2, 0]),
    "nw": np.array([-1, 1]),
}


def read_data(fn):
    input_list = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "")
            input_list += [line]
            line = fp.readline()
    return input_list


def update_black_tile_list(black_tiles, input_str):
    """ returns an list of np arrays """
    pos = np.array([0, 0])
    i = 0
    while i < len(input_str):
        if input_str[i] in DIR_DICT.keys():
            pos += DIR_DICT[input_str[i]]
            # print(input_str[i])
        else:
            i += 1
            pos += DIR_DICT[input_str[i - 1 : i + 1]]
            # print(input_str[i - 1 : i + 1])
        i += 1
    # print("parsed")
    found = False
    i = 0
    while (not found) & (i < len(black_tiles)):
        found = (black_tiles[i] == pos).all()
        if found:
            break
        i += 1
    if found:
        # print(len(black_tiles))
        black_tiles = [x for x in black_tiles if not (x == pos).all()]
        # print("turning {} back from black to white".format(pos))
        # print(len(black_tiles))
    else:
        # print(len(black_tiles))
        black_tiles += [pos]
        # print("turning {} from white to black".format(pos))
        # print(len(black_tiles))
    return black_tiles


def daily_update(black_tiles):
    # go through list of black tiles. For each of their neighbours, add 1
    neigh_dict = {}
    for bt_pos in black_tiles:
        for dir_v in DIR_DICT.values():
            neigh_pos = "{},{}".format(*(np.array(bt_pos) + dir_v))
            if neigh_pos in neigh_dict.keys():
                neigh_dict[neigh_pos] += 1
            else:
                neigh_dict[neigh_pos] = 1
    # go through list of black tiles again. if they only have 1 black neighbour: flip to white
    # (ie remove from list)
    black_tiles_as_strings = ["{},{}".format(*bt) for bt in black_tiles]
    curr_bt_neighbour_count = [neigh_dict.get(bts, 0) for bts in black_tiles_as_strings]
    can_remain_black = [(x == 1) or (x == 2) for x in curr_bt_neighbour_count]
    # print("can remain black: {}".format(can_remain_black))
    # go through all the neighbours that have received a count of 1 or more and are not
    # on the black tiles list: add if they have two neighbours
    flip_white_to_black = [
        k for k, v in neigh_dict.items() if (v == 2) & (k not in black_tiles_as_strings)
    ]
    # flip black > white
    black_tiles = [bt for bt, remain in zip(black_tiles, can_remain_black) if remain]
    # flip white > black
    black_tiles += [
        [int(x) for x in pos_str.split(",")] for pos_str in flip_white_to_black
    ]
    return black_tiles


@click.command()
@click.argument("fn")
def main(fn):
    input_list = read_data(fn)
    print(len(input_list))
    black_tiles = []
    # place initial tiles
    for i, input_str in enumerate(input_list):
        # print("+++ round {} +++".format(i))
        # print(input_str)
        black_tiles = update_black_tile_list(black_tiles, input_str)
    # change np.array > list in black_tiles
    black_tiles = [x.tolist() for x in black_tiles]
    print("+++ day 0 +++")
    print(len(black_tiles))
    # this is the starting position
    # now do a 100 daily updates
    for i in range(1, 101):
        print("+++ day {} +++".format(i))
        black_tiles = daily_update(black_tiles)
        print(len(black_tiles))


if __name__ == "__main__":
    main()