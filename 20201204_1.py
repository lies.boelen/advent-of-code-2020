import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        cnt = 0
        out = {}
        out[cnt] = {}
        while line:
            if line == "\n":
                cnt += 1
                out[cnt] = {}
            else:
                for el in line.split(" "):
                    k, v = el.split(":")
                    out[cnt][k] = v.rstrip()
            line = fp.readline()
    return out


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    counter_correct = 0
    for v in dat.values():
        ks = list(v.keys())
        if (
            len(set(ks).intersection(["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]))
            == 7
        ):
            counter_correct += 1
    print("Number of correct passports = {}".format(counter_correct))


if __name__ == "__main__":
    main()