import numpy as np
import pandas as pd

# import itertools

import click

# import re
# import sys
# sys.setrecursionlimit(1500)


def read_data(fn):
    # return an array of tiles (144, 10, 10)
    tile_number_list = []
    tile_list = []
    tile = []
    # tilemode = False
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            if line == "\n":
                if tile != []:
                    tile_list += [tile]
            elif line[:4] == "Tile":
                tile_number_list += [int(line.replace("Tile ", "").replace(":", ""))]
                tile = []
            else:
                # add to tile
                tile += [[1 if x == "#" else 0 for x in line.replace("\n", "")]]
            line = fp.readline()
    assert len(tile_number_list) == len(tile_list), (
        len(tile_number_list),
        len(tile_list),
    )
    return np.array(tile_number_list), np.array(tile_list, dtype=np.int8)


def get_side(tile, s):
    if s == 0:
        return tile[0]
    elif s == 1:
        return tile[:, -1]
    elif s == 2:
        return tile[-1]
    elif s == 3:
        return tile[:, 0]
    else:
        print("wrong side argument {}".format(s))


def create_match_matrix(tile_array):
    n_tile, _, _ = tile_array.shape
    out = np.zeros((n_tile, 4, n_tile, 4))
    out_rev = np.zeros((n_tile, 4, n_tile, 4))
    for ti in range(n_tile):
        tile1 = tile_array[ti]
        for si in range(4):
            side1 = get_side(tile1, si)
            for tii in range(n_tile):
                tile2 = tile_array[tii]
                if ti != tii:
                    for sii in range(4):
                        side2 = get_side(tile2, sii)
                        if (side1 == side2).all():
                            out[ti, si, tii, sii] = 1
                        elif (side1 == side2[::-1]).all():
                            out_rev[ti, si, tii, sii] = 1
    return out, out_rev


def find_number_matching_sides(B):
    return (B.sum(axis=(2, 3)) > 0).sum(axis=-1)  # (tile,)


def identity(A):
    return A


def rot90_then_fh(A):
    return flip_horizontally(rotate90(A))


def rot90_then_fv(A):
    return flip_vertically(rotate90(A))


def rotate90(A):
    # will only work for square matrices
    out = np.zeros_like(A, dtype=np.int8)
    for i in range(A.shape[0]):
        out[:, A.shape[0] - i - 1] = A[i]
    return out


def rotate180(A):
    return rotate90(rotate90(A))


def rotate270(A):
    return rotate90(rotate180(A))


def flip_horizontally(A):
    return A[::-1, :]


def flip_vertically(A):
    return A[:, ::-1]


TRANSFORMATIONS = [
    [identity, lambda x: x],
    [rotate90, lambda x: x[[3, 0, 1, 2]]],
    [rotate180, lambda x: x[[2, 3, 0, 1]]],
    [rotate270, lambda x: x[[1, 2, 3, 0]]],
    [flip_horizontally, lambda x: x[[2, 1, 0, 3]]],
    [flip_vertically, lambda x: x[[0, 3, 2, 1]]],
    [rot90_then_fv, lambda x: x[[3, 2, 1, 0]]],
    [rot90_then_fh, lambda x: x[[1, 0, 3, 2]]],
]


def solve_puzzle(
    tile_array, tile_number_array, corner_mask, border_mask, ts_match_count
):
    n_tiles, tile_size, _ = tile_array.shape
    side_length = np.sqrt(n_tiles).astype(int)
    puzzle = (
        np.ones((side_length * tile_size, side_length * tile_size), dtype=np.int8) * 2
    )
    already_used_mask = np.zeros(n_tiles, dtype=bool)
    # put one corner piece in the upper left
    ul_piece_idx = list(corner_mask).index(True)
    tile = tile_array[ul_piece_idx]
    match_count = ts_match_count[ul_piece_idx]
    match_wanted = np.array([0, 1, 1, 0])
    for f1, f2 in TRANSFORMATIONS:
        if (f2(match_count) == match_wanted).all():
            tile_transformed = f1(tile)
            break
    # mark piece as used
    already_used_mask[ul_piece_idx] = True
    # remove it from the border mask
    corner_mask[ul_piece_idx] = False
    # add piece to the puzzle
    puzzle[:tile_size, :tile_size] = tile_transformed
    # print_puzzle(puzzle, add_delim=True)

    # fill in the top row - 10 border pieces + 1 corner piece
    match_wanted = np.array([0, 1, 1, 1])
    for k in range(1, side_length - 1):
        border_idxs = np.where(border_mask)[0]
        possible_fits = []
        for border_idx in border_idxs:
            tile = tile_array[border_idx]
            match_count = ts_match_count[border_idx]
            for f1, f2 in TRANSFORMATIONS:
                cond1 = (f2(match_count) == match_wanted).all()
                tile_transformed = f1(tile)
                cond2 = (
                    tile_transformed[:, 0] == puzzle[:tile_size, k * tile_size - 1]
                ).all()
                if cond1 & cond2:
                    possible_fits += [[border_idx, tile_transformed]]
        assert len(possible_fits) == 1, (k, possible_fits)
        puzzle[:tile_size, (k * tile_size) : ((k + 1) * tile_size)] = possible_fits[0][
            1
        ]
        # print("* * *")
        # print_puzzle(puzzle, add_delim=True)
        already_used_mask[possible_fits[0][0]] = True
        border_mask[possible_fits[0][0]] = False
    # upper right corner
    corner_idxs = np.where(corner_mask)[0]
    match_wanted = np.array([0, 0, 1, 1])
    possible_fits = []
    for corner_idx in corner_idxs:
        tile = tile_array[corner_idx]
        match_count = ts_match_count[corner_idx]
        for f1, f2 in TRANSFORMATIONS:
            cond1 = (f2(match_count) == match_wanted).all()
            tile_transformed = f1(tile)
            cond2 = (
                tile_transformed[:, 0]
                == puzzle[:tile_size, (side_length - 1) * tile_size - 1]
            ).all()
            if cond1 & cond2:
                possible_fits += [[corner_idx, tile_transformed]]
    assert len(possible_fits) == 1, possible_fits
    puzzle[:tile_size, -tile_size:] = possible_fits[0][1]
    # print("* * *")
    # print_puzzle(puzzle, add_delim=True)
    already_used_mask[possible_fits[0][0]] = True
    corner_mask[possible_fits[0][0]] = False

    # fill in the left column - 10 border pieces + 1 corner piece
    match_wanted = np.array([1, 1, 1, 0])
    for k in range(1, side_length - 1):
        border_idxs = np.where(border_mask)[0]
        possible_fits = []
        for border_idx in border_idxs:
            tile = tile_array[border_idx]
            match_count = ts_match_count[border_idx]
            for f1, f2 in TRANSFORMATIONS:
                cond1 = (f2(match_count) == match_wanted).all()
                tile_transformed = f1(tile)
                cond2 = (
                    tile_transformed[0, :] == puzzle[k * tile_size - 1, :tile_size]
                ).all()
                if cond1 & cond2:
                    possible_fits += [[border_idx, tile_transformed]]
        assert len(possible_fits) == 1, (k, possible_fits)
        puzzle[(k * tile_size) : ((k + 1) * tile_size), :tile_size] = possible_fits[0][
            1
        ]
        # print("* * *")
        # print_puzzle(puzzle, add_delim=True)
        already_used_mask[possible_fits[0][0]] = True
        border_mask[possible_fits[0][0]] = False
    # bottom left corner
    corner_idxs = np.where(corner_mask)[0]
    match_wanted = np.array([1, 1, 0, 0])
    possible_fits = []
    for corner_idx in corner_idxs:
        tile = tile_array[corner_idx]
        match_count = ts_match_count[corner_idx]
        for f1, f2 in TRANSFORMATIONS:
            cond1 = (f2(match_count) == match_wanted).all()
            tile_transformed = f1(tile)
            cond2 = (
                tile_transformed[0, :]
                == puzzle[(side_length - 1) * tile_size - 1, :tile_size]
            ).all()
            if cond1 & cond2:
                possible_fits += [[corner_idx, tile_transformed]]
    assert len(possible_fits) == 1, possible_fits
    puzzle[-tile_size:, :tile_size] = possible_fits[0][1]
    # print("* * *")
    # print_puzzle(puzzle, add_delim=True)
    already_used_mask[possible_fits[0][0]] = True
    corner_mask[possible_fits[0][0]] = False

    # fill in the bottom row
    match_wanted = np.array([1, 1, 0, 1])
    for k in range(1, side_length - 1):
        border_idxs = np.where(border_mask)[0]
        possible_fits = []
        for border_idx in border_idxs:
            tile = tile_array[border_idx]
            match_count = ts_match_count[border_idx]
            for f1, f2 in TRANSFORMATIONS:
                cond1 = (f2(match_count) == match_wanted).all()
                tile_transformed = f1(tile)
                cond2 = (
                    tile_transformed[:, 0] == puzzle[-tile_size:, k * tile_size - 1]
                ).all()
                if cond1 & cond2:
                    possible_fits += [[border_idx, tile_transformed]]
        assert len(possible_fits) == 1, (k, possible_fits)
        puzzle[-tile_size:, (k * tile_size) : ((k + 1) * tile_size)] = possible_fits[0][
            1
        ]
        # print("* * *")
        # print_puzzle(puzzle, add_delim=True)
        already_used_mask[possible_fits[0][0]] = True
        border_mask[possible_fits[0][0]] = False
    # bottom right corner - leave for later

    # fill in the right row
    match_wanted = np.array([1, 0, 1, 1])
    for k in range(1, side_length - 1):
        border_idxs = np.where(border_mask)[0]
        possible_fits = []
        for border_idx in border_idxs:
            tile = tile_array[border_idx]
            match_count = ts_match_count[border_idx]
            for f1, f2 in TRANSFORMATIONS:
                cond1 = (f2(match_count) == match_wanted).all()
                tile_transformed = f1(tile)
                cond2 = (
                    tile_transformed[0, :] == puzzle[k * tile_size - 1, -tile_size:]
                ).all()
                if cond1 & cond2:
                    possible_fits += [[border_idx, tile_transformed]]
        assert len(possible_fits) == 1, (k, possible_fits)
        puzzle[(k * tile_size) : ((k + 1) * tile_size), -tile_size:] = possible_fits[0][
            1
        ]
        # print("* * *")
        # print_puzzle(puzzle, add_delim=True)
        already_used_mask[possible_fits[0][0]] = True
        border_mask[possible_fits[0][0]] = False

    # now check whether the last corner piece fits
    br_piece_idx = list(corner_mask).index(True)
    match_wanted = np.array([1, 0, 0, 1])
    tile = tile_array[br_piece_idx]
    match_count = ts_match_count[br_piece_idx]
    possible_fits = []
    for f1, f2 in TRANSFORMATIONS:
        cond1 = (f2(match_count) == match_wanted).all()
        tile_transformed = f1(tile)
        cond2 = (
            tile_transformed[0, :]
            == puzzle[(side_length - 1) * tile_size - 1, -tile_size:]
        ).all()
        cond3 = (
            tile_transformed[:, 0]
            == puzzle[-tile_size:, (side_length - 1) * tile_size - 1]
        ).all()
        if cond1 & cond2 & cond3:
            possible_fits += [[br_piece_idx, tile_transformed]]
            break
    assert len(possible_fits) == 1, possible_fits
    puzzle[-tile_size:, -tile_size:] = possible_fits[0][1]
    already_used_mask[possible_fits[0][0]] = True
    corner_mask[possible_fits[0][0]] = False

    # row 1 - 10 (with zero-indexing)
    for i in range(1, side_length - 1):
        for j in range(1, side_length - 1):
            possible_fits = []
            possible_idxs = np.where(already_used_mask == False)[0]
            for possible_idx in possible_idxs:
                tile = tile_array[possible_idx]
                for f1, _ in TRANSFORMATIONS:
                    tile_transformed = f1(tile)
                    cond_top = (
                        tile_transformed[0, :]
                        == puzzle[
                            tile_size * i - 1, (tile_size * j) : (tile_size * (j + 1))
                        ]
                    ).all()
                    cond_left = (
                        tile_transformed[:, 0]
                        == puzzle[
                            (tile_size * i) : (tile_size * (i + 1)), tile_size * j - 1
                        ]
                    ).all()
                    if (i < side_length - 2) & (j < side_length - 2):
                        if cond_top & cond_left:
                            possible_fits += [[possible_idx, tile_transformed]]
                    elif (i < side_length - 2) & (j == side_length - 2):
                        cond_right = (
                            tile_transformed[:, -1]
                            == puzzle[
                                (tile_size * i) : (tile_size * (i + 1)),
                                tile_size * (j + 1),
                            ]
                        ).all()
                        if cond_top & cond_left & cond_right:
                            possible_fits += [[possible_idx, tile_transformed]]
                    elif (i == side_length - 2) & (j < side_length - 2):
                        cond_bottom = (
                            tile_transformed[-1, :]
                            == puzzle[
                                tile_size * (i + 1),
                                (tile_size * j) : (tile_size * (j + 1)),
                            ]
                        ).all()
                        if cond_top & cond_left & cond_bottom:
                            possible_fits += [[possible_idx, tile_transformed]]
                    else:  # i == j == side_length - 2
                        cond_right = (
                            tile_transformed[:, -1]
                            == puzzle[
                                (tile_size * i) : (tile_size * (i + 1)),
                                tile_size * (j + 1),
                            ]
                        ).all()
                        cond_bottom = (
                            tile_transformed[-1, :]
                            == puzzle[
                                tile_size * (i + 1),
                                (tile_size * j) : (tile_size * (j + 1)),
                            ]
                        ).all()
                        if cond_top & cond_left & cond_right & cond_bottom:
                            possible_fits += [[possible_idx, tile_transformed]]
            assert len(possible_fits) == 1, (i, j)
            puzzle[
                (tile_size * i) : (tile_size * (i + 1)),
                (tile_size * j) : (tile_size * (j + 1)),
            ] = possible_fits[0][1]
            already_used_mask[possible_fits[0][0]] = True

    return puzzle


def prune_puzzle(puzzle, tile_size, side_len):
    """
    removing all the borders of the tiles
    """
    all_idxs = np.arange(tile_size * side_len)
    bad_idxs = np.concatenate(
        [
            np.arange(side_len) * tile_size,
            np.arange(side_len) * tile_size + (tile_size - 1),
        ]
    )
    good_idxs = [x for x in all_idxs if x not in bad_idxs]
    puzzle = puzzle[good_idxs]
    puzzle = puzzle[:, good_idxs]
    return puzzle


def get_monster():
    monster = """                  # 
#    ##    ##    ###
 #  #  #  #  #  #   """.replace(
        "#", "1"
    ).replace(
        " ", "0"
    )
    return np.array([list(v) for v in monster.split("\n")]).astype(int)


def tag_monsters(puzzle, monster):
    monster_size = monster.sum()
    m0, m1 = monster.shape
    p0, p1 = puzzle.shape
    for i in range(p0 - m0 + 1):
        for j in range(p1 - m1 + 1):
            if (puzzle[i : i + m0, j : j + m1] * monster).sum() == monster_size:
                puzzle[i : i + m0, j : j + m1] += 99 * monster
    return puzzle


def print_puzzle(puzzle, add_delim=False):
    puzzle_size = puzzle.shape[0]
    puzzle_as_list = puzzle.tolist()
    for i, sublist in enumerate(puzzle_as_list):
        string = ""
        for j, v in enumerate(sublist):
            if v == 0:
                string += "."
            elif v == 1:
                string += "#"
            elif v == 100:
                string += "0"
            elif v == 2:
                string += " "
            else:
                print("DRAMA, found {}".format(v))
            if add_delim & (np.mod(j, 10) == 9):
                string += "|"
        print(string)
        if (np.mod(i, 10) == 9) & add_delim:
            print("{}".format("".join([""] * puzzle_size)))


@click.command()
@click.argument("fn")
def main(fn):
    tile_number_array, tile_array = read_data(fn)
    number_tiles = len(tile_array)
    side_length = np.sqrt(number_tiles).astype(int)
    tile_size = tile_array.shape[1]
    # create matrix which counts the number of possible matching sides
    A, A_rev = create_match_matrix(tile_array)
    B = A + A_rev
    # (tile,)
    number_matching_sides = find_number_matching_sides(B)
    if (number_matching_sides == 2).sum() == 4:
        corner_mask = number_matching_sides == 2
    if (number_matching_sides == 3).sum() == 4 * (side_length - 2):
        border_mask = number_matching_sides == 3
    else:
        print("too many border tiles")
    # (tile, side) : number of matches
    ts_match_count = (B.sum(axis=(2, 3)) > 0).astype(np.int8)
    puzzle = solve_puzzle(
        tile_array, tile_number_array, corner_mask, border_mask, ts_match_count
    )
    # print_puzzle(puzzle, add_delim=True)
    # print("++++++++")
    # remove all of the borders
    puzzle = prune_puzzle(puzzle, tile_size, side_length)
    # print_puzzle(puzzle)
    monster = get_monster()
    # scan for monsters
    for i, (f1, _) in enumerate(TRANSFORMATIONS):
        print("trying transformation {} on puzzle".format(i))
        # transform puzzle
        puzzle_transformed = f1(puzzle.copy())
        # look for monsters
        monster_puzzle = tag_monsters(puzzle_transformed, monster)
        # if monsters found, count roughness
        if (monster_puzzle == 100).sum() > 0:
            roughness_count = (monster_puzzle == 1).sum()
            print_puzzle(monster_puzzle)
            break

    print("Answer = {}".format(roughness_count))


if __name__ == "__main__":
    main()