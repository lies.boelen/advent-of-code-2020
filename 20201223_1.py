import pandas as pd
import numpy as np


def play_one_round(game_status, current_idx):
    N = len(game_status)
    # get value of current cup
    current_val = game_status[current_idx]
    # take out next three cups
    take_out_vals = (game_status + game_status)[(current_idx + 1) : (current_idx + 4)]
    remaining_vals = [x for x in game_status if x not in take_out_vals]
    print("taking out: {}".format(take_out_vals))
    # find the destination value
    destination_val = current_val - 1
    while destination_val not in remaining_vals:
        destination_val = destination_val - 1 if destination_val > 1 else N
    print("inserting next to {}".format(destination_val))
    # find destination idx
    destination_idx = remaining_vals.index(destination_val)
    # add one because the cups are going after it
    insertion_idx = np.mod(destination_idx + 1, len(remaining_vals))
    new_status = (
        remaining_vals[:insertion_idx] + take_out_vals + remaining_vals[insertion_idx:]
    )
    current_idx = new_status.index(current_val)
    # print(destination_idx, insertion_idx)
    # insert cups taken out
    return (
        new_status,
        np.mod(current_idx + 1, N),
    )


def print_game_status(game_status, current_idx):
    out_str = ""
    for i in range(len(game_status)):
        out_str += (
            "({}) ".format(game_status[i])
            if i == current_idx
            else "{} ".format(game_status[i])
        )
    print(out_str)


def main():
    input_str = "586439172"
    # input_str = "389125467"  # example online
    game_status = [int(x) for x in input_str]
    current_idx = 0
    for i in range(100):
        print("+++ round {} +++".format(i))
        print_game_status(game_status, current_idx)
        game_status, current_idx = play_one_round(game_status, current_idx)
    print("+++ end of game +++")
    print_game_status(game_status, current_idx)
    # labels after cup 1
    one_idx = game_status.index(1)
    N = len(game_status)
    game_status_string = [str(x) for x in game_status]
    print("".join((game_status_string + game_status_string)[one_idx + 1 : one_idx + N]))


if __name__ == "__main__":
    main()