import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return as numpy arrays:
    # 1. seat (n_seats,)
    # 2. neigbhour matrix (n_seats, n_seats)
    seat_list = []
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        while line:
            line_ = line.replace("\n", "").replace("L", "0").replace(".", "2")
            seat_list += [list(line_)]
            line = fp.readline()
    seat_array_2d = np.array(seat_list).astype(np.int8)
    seat_array_1d = seat_array_2d.flatten()
    n_rows, n_cols = seat_array_2d.shape
    n_seats = len(seat_array_1d)
    neighbour_matrix = np.zeros((n_seats, n_seats))
    # add neighbours on the W
    for i in range(n_rows):  # all rows
        for j in range(1, n_cols):  # all columns except leftmost
            neighbour_matrix[(i * n_cols) + j, (i * n_cols) + (j - 1)] = 1
    # add neighbours on the E
    for i in range(n_rows):  # all rows
        for j in range(n_cols - 1):  # all columns except rightmost
            neighbour_matrix[(i * n_cols) + j, (i * n_cols) + (j + 1)] = 1
    # add neighbours on the N
    for i in range(1, n_rows):  # all rows except top one
        for j in range(n_cols):  # all columns
            neighbour_matrix[(i * n_cols) + j, ((i - 1) * n_cols) + j] = 1
    # add neighbours on the S
    for i in range(n_rows - 1):  # all rows except bottom one
        for j in range(n_cols):  # all columns
            neighbour_matrix[(i * n_cols) + j, ((i + 1) * n_cols) + j] = 1
    # add neighbours on the NE
    for i in range(1, n_rows):  # all rows except top
        for j in range(n_cols - 1):  # all columns except rightmost
            neighbour_matrix[(i * n_cols) + j, ((i - 1) * n_cols) + (j + 1)] = 1
    # add neighbours on the NW
    for i in range(1, n_rows):  # all rows except top
        for j in range(1, n_cols):  # all columns except leftmost
            neighbour_matrix[(i * n_cols) + j, ((i - 1) * n_cols) + (j - 1)] = 1
    # add neighbours on the SE
    for i in range(n_rows - 1):  # all rows except bottom
        for j in range(n_cols - 1):  # all columns except rightmost
            neighbour_matrix[(i * n_cols) + j, ((i + 1) * n_cols) + (j + 1)] = 1
    # add neighbours on the SW
    for i in range(n_rows - 1):  # all rows except bottom
        for j in range(1, n_cols):  # all columns except leftmost
            neighbour_matrix[(i * n_cols) + j, ((i + 1) * n_cols) + (j - 1)] = 1

    # from seat array and neighbour matrix, remove floor spots
    is_chair_mask = seat_array_1d == 0
    seat_array_1d = seat_array_1d[is_chair_mask]
    neighbour_matrix = neighbour_matrix[np.ix_(is_chair_mask, is_chair_mask)]

    return seat_array_1d, neighbour_matrix


def update_sitting_situation(seat_array_1d, neighbour_matrix):
    # (cond1) find empty seats that have no occupied seats adjacent to it
    # (cond2) find occupied seats with >= 4 adjacent seats taken
    #
    neighbour_count = neighbour_matrix @ seat_array_1d
    cond1 = np.where(neighbour_count >= 4)[0]
    cond2 = np.where(neighbour_count == 0)[0]
    new_seating = seat_array_1d.copy()
    new_seating[cond1] = 0
    new_seating[cond2] = 1
    return new_seating


def run_sim_until_stable(seat_array_1d, neighbour_matrix):
    while True:
        seat_array_1d_old = seat_array_1d.copy()
        seat_array_1d = update_sitting_situation(seat_array_1d, neighbour_matrix)
        if (seat_array_1d == seat_array_1d_old).all():
            return (seat_array_1d == 1).sum()


@click.command()
@click.argument("fn")
def main(fn):
    seat_array_1d, neighbour_matrix = read_data(fn)
    seats_occupied_in_stable = run_sim_until_stable(seat_array_1d, neighbour_matrix)
    print(
        "Number of seats occupied in stable situation = {}".format(
            seats_occupied_in_stable
        )
    )


if __name__ == "__main__":
    main()