import numpy as np
import pandas as pd

import itertools

import click

# import re
# import sys
# sys.setrecursionlimit(1500)

DIR_DICT = {
    "ne": np.array([1, 1]),
    "e": np.array([2, 0]),
    "se": np.array([1, -1]),
    "sw": np.array([-1, -1]),
    "w": np.array([-2, 0]),
    "nw": np.array([-1, 1]),
}


def read_data(fn):
    input_list = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "")
            input_list += [line]
            line = fp.readline()
    return input_list


def update_black_tile_list(black_tiles, input_str):
    pos = np.array([0, 0])
    i = 0
    while i < len(input_str):
        if input_str[i] in DIR_DICT.keys():
            pos += DIR_DICT[input_str[i]]
            # print(input_str[i])
        else:
            i += 1
            pos += DIR_DICT[input_str[i - 1 : i + 1]]
            # print(input_str[i - 1 : i + 1])
        i += 1
    # print("parsed")
    found = False
    i = 0
    while (not found) & (i < len(black_tiles)):
        found = (black_tiles[i] == pos).all()
        if found:
            break
        i += 1
    if found:
        # print(len(black_tiles))
        black_tiles = [x for x in black_tiles if not (x == pos).all()]
        # print("turning {} back from black to white".format(pos))
        # print(len(black_tiles))
    else:
        # print(len(black_tiles))
        black_tiles += [pos]
        # print("turning {} from white to black".format(pos))
        # print(len(black_tiles))
    return black_tiles


@click.command()
@click.argument("fn")
def main(fn):
    input_list = read_data(fn)
    print(len(input_list))
    black_tiles = []
    for i, input_str in enumerate(input_list):
        print("+++ round {} +++".format(i))
        print(input_str)
        black_tiles = update_black_tile_list(black_tiles, input_str)
    print("Number of black tiles = {}".format(len(black_tiles)))


if __name__ == "__main__":
    main()