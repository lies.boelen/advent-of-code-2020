import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    out = {}
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        while line != "":
            linesplit = line.split(" bags contain ")
            outer_colour = linesplit[0]
            out[outer_colour] = {}
            inside = linesplit[1]
            if inside == "no other bags.\n":
                pass
            else:
                inside = (
                    inside.replace(" bags", "").replace(" bag", "").replace(".\n", "")
                )
                inside_split = inside.split(", ")
                for k in inside_split:
                    num, col1, col2 = k.split(" ")
                    out[outer_colour]["{} {}".format(col1, col2)] = int(num)
            line = fp.readline()
    key_colours = list(out.keys())
    inside_colours = [list(v.keys()) for v in out.values()]
    all_colours = key_colours + [
        colour for sublist in inside_colours for colour in sublist
    ]
    all_colours_unique = pd.Series(all_colours).unique()  # np array
    container_df = pd.DataFrame(0, index=all_colours_unique, columns=all_colours_unique)
    for k, v in out.items():
        if len(v) > 0:
            for k1, v1 in v.items():
                container_df.loc[k, k1] = v1

    return container_df


@click.command()
@click.argument("fn")
@click.option("--calc-df", default=True)
def main(fn, calc_df):
    container_df_filename = "20201207_container_df.csv"
    if calc_df:
        container_df = read_data(fn)
        container_df.to_csv(container_df_filename)
    else:
        container_df = pd.read_csv(container_df_filename)
    # print(container_df)
    index_colours = container_df.index.values  # np array
    # print(index_colours)
    k = 0
    container_colour_dict = {}
    ref_cols = ["shiny gold"]
    while len(ref_cols) > 0:
        container_colour_dict[k] = []
        for rc in ref_cols:
            vals = container_df[rc].values
            container_colour_dict[k] += list(index_colours[vals > 0])
        ref_cols = container_colour_dict[k]
        k += 1

    all_possible_containing_colours = np.unique(
        np.array(
            [colour for sublist in container_colour_dict.values() for colour in sublist]
        )
    )

    print(
        "Total possible colours containing my bag = {}".format(
            len(all_possible_containing_colours)
        )
    )
    print(all_possible_containing_colours)


if __name__ == "__main__":
    main()