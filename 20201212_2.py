import numpy as np
import pandas as pd

import click


direction_dict = {
    "N": np.array([0, 1]),
    "E": np.array([1, 0]),
    "S": np.array([0, -1]),
    "W": np.array([-1, 0]),
}

R_string = "NESW"
L_string = "ENWS"


def read_data(fn):
    # return as a list
    return pd.read_csv(fn, header=None).values.flatten().tolist()


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # instructions as a list

    ship_position = np.array([0, 0])
    waypoint_position = np.array([10, 1])  # relative to the ship's position

    for d in dat:
        print(d)
        movement = d[0]
        size = int(d[1:])
        if movement in direction_dict.keys():
            waypoint_position += direction_dict[movement] * size
        elif movement == "F":
            ship_position += waypoint_position * size
        elif movement == "R":
            num_turns = int(size / 90)
            for i in range(num_turns):
                waypoint_position = np.array([[0, 1], [-1, 0]]) @ waypoint_position
        else:  # movement == "L"
            num_turns = int(size / 90)
            for i in range(num_turns):
                waypoint_position = np.array([[0, -1], [1, 0]]) @ waypoint_position

    print("Manhattan distance travelled = {}".format(np.abs(ship_position).sum()))


if __name__ == "__main__":
    main()