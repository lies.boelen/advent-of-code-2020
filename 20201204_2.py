import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        cnt = 0
        out = {}
        out[cnt] = {}
        while line:
            if line == "\n":
                cnt += 1
                out[cnt] = {}
            else:
                for el in line.split(" "):
                    k, v = el.split(":")
                    out[cnt][k] = v.rstrip()
            line = fp.readline()
    return out


def check_validity_year(yr_as_str, lower, upper):
    if not (yr_as_str.isdecimal() | (len(yr_as_str) == 4)):
        return 0
    # year between values
    yr = int(yr_as_str)
    if (yr < lower) | (yr > upper):
        return 0
    return 1


def check_height(hgt):
    num, unit = hgt[:-2], hgt[-2:]
    if not num.isdecimal():
        return 0
    num = int(num)
    if (unit == "cm") & (num >= 150) & (num <= 193):
        return 1
    if (unit == "in") & (num >= 59) & (num <= 76):
        return 1
    return 0


def ishex(hcl):
    if hcl[0] != "#":
        return 0
    if len(hcl) != 7:
        return 0
    for i in range(1, 7):
        if not (hcl[i] in "1234567890abcdef"):
            return 0
    return 1


def check_eye_colour(ecl):
    return ecl in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]


def check_pid(pid):
    return int(pid.isdecimal() & (len(pid) == 9))


def check_keys(keys_list):
    return int(
        len(
            set(keys_list).intersection(
                ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]
            )
        )
        == 7
    )


def check_validity(d):
    """ returns 1 if valid, 0 if invalid """
    # all fields available?
    if check_keys(list(d.keys())) == 0:
        return 0
    # birth yr checks
    if check_validity_year(d["byr"], 1920, 2002) == 0:
        return 0
    # issue yr checks
    if check_validity_year(d["iyr"], 2010, 2020) == 0:
        return 0
    # expiration yr check
    if check_validity_year(d["eyr"], 2020, 2030) == 0:
        return 0
    # check height
    if check_height(d["hgt"]) == 0:
        return 0
    # check hair colour
    if ishex(d["hcl"]) == 0:
        return 0
    # check eye colour
    if check_eye_colour(d["ecl"]) == 0:
        return 0
    # check pid
    if check_pid(d["pid"]) == 0:
        return 0
    return 1


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    counter_correct = 0
    for v in dat.values():
        counter_correct += check_validity(v)
    print("Number of correct passports = {}".format(counter_correct))


if __name__ == "__main__":
    main()