import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as f:
        inp = f.read()
        inp = inp.replace(".", "0").replace("#", "1")
        inp = np.array([list(x) for x in inp.split("\n")]).astype(np.int8)
        return inp


@click.command()
@click.option("--right", default=3)
@click.option("--down", default=1)
@click.argument("fn")
def main(right, down, fn):
    arr = read_data(fn)  # get np array
    nrow, ncol = arr.shape
    print("input shape @{}".format(arr.shape))
    num_steps = int(nrow / down)
    print("number of steps = {}".format(num_steps))
    rpos = np.arange(num_steps) * down
    cpos = np.mod(np.arange(num_steps) * right, ncol)
    print("rpos = {}".format(rpos))
    print("cpos = {}".format(cpos))
    print("number of trees met = {}".format(arr[rpos, cpos].sum()))


if __name__ == "__main__":
    prod = main()