import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    alphabet_list = list(string.ascii_lowercase)
    letter_to_index = {k: v for v, k in enumerate(alphabet_list)}
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        grp = 0
        out = {}
        out[grp] = [0] * 26
        grp_sizes = []
        grp_size_counter = 0
        while line:
            if line == "\n":
                grp_sizes += [grp_size_counter]
                grp_size_counter = 0
                grp += 1
                out[grp] = [0] * 26
            else:
                for letter in line.replace("\n", ""):
                    out[grp][letter_to_index[letter]] += 1
                grp_size_counter += 1
            line = fp.readline()
            print(line)
        grp_sizes += [grp_size_counter]
    out_array = np.array(list(out.values()))
    grp_sizes = np.array(grp_sizes)
    print("out_array @{}".format(out_array.shape))
    print("grp_sizes @{}".format(grp_sizes.shape))
    return out_array, grp_sizes


@click.command()
@click.argument("fn")
def main(fn):
    yes_counter, grp_sizes = read_data(fn)
    print(yes_counter)
    print(grp_sizes)
    all_yes_per_group = yes_counter == grp_sizes[:, None]
    print("Total sum of all yes per group = {}".format(all_yes_per_group.sum()))


if __name__ == "__main__":
    main()