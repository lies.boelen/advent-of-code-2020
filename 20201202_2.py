import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as f:
        return f.read().split("\n")


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    counter_correct = 0
    for d in dat:
        if len(d) > 0:
            positions, letter, pw = d.split(" ")
            pos1, pos2 = positions.split("-")
            pos1 = int(pos1) - 1  # zero indexing
            pos2 = int(pos2) - 1  # zero indexing
            letter = letter[0]
            pw_array = np.array(list(pw))
            if (pw_array[[pos1, pos2]] == letter).sum() == 1:
                counter_correct += 1
    print("Number of correct passwords = {}".format(counter_correct))


if __name__ == "__main__":
    prod = main()