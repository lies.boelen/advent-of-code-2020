import numpy as np
import pandas as pd

import itertools

import click

# import re
# import sys
# sys.setrecursionlimit(1500)


def read_data(fn):
    # returns XXX
    input_list = []
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            line = line.replace("\n", "").replace(")", "").replace(",", "")
            ingredients, allergens = line.split(" (contains ")
            input_list += [[ingredients.split(" "), allergens.split(" ")]]
            line = fp.readline()
    return input_list


def get_all_ingredients(input_list):
    ingredients = [x[0] for x in input_list]
    ingredients = list(set([y for x in ingredients for y in x]))
    return ingredients


def get_all_allergens(input_list):
    allergens = [x[1] for x in input_list]
    allergens = list(set([y for x in allergens for y in x]))
    return allergens


def accrue_mentions(input_list, ingredients, allergens):
    """
    Return a matrix with dims (ingredients, allergens) of which the entries are
    the number of times the (ingredient, allergen) combo has been mentioned together
    """
    out = np.zeros((len(ingredients), len(allergens)), dtype=np.int32)
    for ing_list, all_list in input_list:
        ing_idxs = [ingredients.index(x) for x in ing_list]
        all_idxs = [allergens.index(x) for x in all_list]
        for x in itertools.product(ing_idxs, all_idxs):
            out[x] += 1
    return out


def prune(arr):
    """
    make arr one-hot on the columns
    """
    while (arr.sum(axis=0) > 1).any():
        # find all cols which have only one 1
        good_cols_bool = arr.sum(axis=0) == 1
        good_cols = np.where(good_cols_bool)[0]
        # set the column where that 1 occurs to zero for all other columns
        for gc in good_cols:
            row = np.where(arr[:, gc] == 1)[0][0]
            arr[
                row,
                ~good_cols_bool,
            ] = 0
    assert (arr.sum(axis=0) == 1).all(), "prune fail"
    return arr


def solve(mention_matrix, ingredients, allergens):
    max_mentions = mention_matrix.max(axis=0)
    # A(ingredient, allergen) is True is the ingredient is listed
    # in all recipes mentioning  the allergen
    A = mention_matrix == max_mentions[None, :]
    # check whether the ingredient never could be containing the allergen
    mask_possible_match = A.sum(axis=-1) > 0
    # keep only ingredients that could have the allergen
    A = A[mask_possible_match]
    remaining_ingredients = np.array(ingredients)[mask_possible_match]
    # prune the matrix
    B = prune(A)
    r, c = np.where(B)
    ided_ingredients = np.array(remaining_ingredients)[r]
    ided_allergens = np.array(allergens)[c]
    ser = pd.Series(ided_allergens, index=ided_ingredients).sort_values(ascending=True)
    print(ser)
    print(",".join(ser.index))
    # return ingredient_mention_counter[mask_impossible_match].sum()


@click.command()
@click.argument("fn")
def main(fn):
    input_list = read_data(fn)
    all_ingredients = get_all_ingredients(input_list)
    all_allergens = get_all_allergens(input_list)
    mention_matrix = accrue_mentions(input_list, all_ingredients, all_allergens)
    solve(mention_matrix, all_ingredients, all_allergens)


if __name__ == "__main__":
    main()