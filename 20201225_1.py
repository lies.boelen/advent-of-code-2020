import numpy as np
import pandas as pd

import itertools

import click


def find_loop_size(subject_number, final_value):
    i = 1
    value = subject_number
    final_value = np.mod(final_value, 20201227)
    while (value != final_value) & (i <= 20201227):
        print(i, value)
        i += 1
        value = np.mod(value * subject_number, 20201227)
    return i


def apply_encryption(subject_number, num_loops):
    value = subject_number
    for i in range(1, num_loops):
        value = np.mod(value * subject_number, 20201227)
    return value


def main():
    card_pk, door_pk = 1965712, 19072108
    # card_pk, door_pk = 5764801, 17807724  # example online
    card_loop_size = find_loop_size(7, card_pk)
    print("card loop size = {}".format(card_loop_size))
    # door_loop_size = find_loop_size(7, door_pk)
    card_encryption_key = apply_encryption(door_pk, card_loop_size)
    # door_encryption_key = apply_encryption(card_pk, door_loop_size)
    print(card_encryption_key)


if __name__ == "__main__":
    main()