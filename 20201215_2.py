import numpy as np
import pandas as pd

import click

import string


def main():
    start = np.array([12, 1, 16, 3, 11, 0], dtype=int)
    len_game = 30_000_000
    last_seen = np.ones(len_game, dtype=np.int64) * (-1)
    for i, val in enumerate(start):
        last_seen[val] = i
    curr_val = start[-1]
    print(curr_val, last_seen[curr_val])
    for k in range(len(start), len_game):
        if np.mod(k, 10_000):
            print(k)
        # print("last seen = {}".format(last_seen[curr_val]))
        # print(curr_val, last_seen[curr_val])
        if last_seen[curr_val] == -1:
            # curr_val_new = 0
            last_seen[curr_val] = k - 1
            curr_val = 0
        else:
            curr_val_new = np.int64(k - 1 - last_seen[curr_val])
            last_seen[curr_val] = k - 1
            curr_val = curr_val_new
    print("{}th number in game = {}".format(len_game, curr_val))


if __name__ == "__main__":
    main()
