import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return
    # 1. rules
    # 2. array your ticket
    # 3. array: other tickets
    with open(fn, "r") as fp:
        line = fp.readline().replace("\n", "")
        setting = "rules"
        rule_dict = {}
        nearby_ticks_list = []
        while line:
            line = line.replace("\n", "")
            if setting == "rules":
                if line != "":
                    a, b = line.split(": ")
                    c, d = b.split(" or ")
                    c1, c2 = c.split("-")
                    d1, d2 = d.split("-")
                    rule_dict[a] = [[int(c1), int(c2)], [int(d1), int(d2)]]
                else:
                    setting = "my_ticket"

            elif setting == "my_ticket":
                if (line != "") & (line != "your ticket:"):
                    my_tick_array = np.array(
                        [int(x) for x in line.split(",")], dtype=int
                    )
                if line == "":
                    setting = "nearby_tickets"

            elif setting == "nearby_tickets":
                if (line != "") & (line != "nearby tickets:"):
                    nearby_ticks_list += [line.split(",")]

            line = fp.readline()

    nearby_ticks_array = np.array(nearby_ticks_list, dtype=int)
    return rule_dict, my_tick_array, nearby_ticks_array


def process_rule_dict_to_array(rule_dict):
    """
    return an array of numbers that are allowed on tickets (over all fields)
    """
    # first, find the max of all rules
    max_val_allowed = 0
    for v in rule_dict.values():
        if v[0][-1] > max_val_allowed:
            max_val_allowed = v[0][-1]
        if v[1][-1] > max_val_allowed:
            max_val_allowed = v[1][-1]
    allowed_arr = np.zeros(max_val_allowed + 1, dtype=np.int8)
    for v in rule_dict.values():
        allowed_arr[v[0][0] : (v[0][1] + 1)] = 1
        allowed_arr[v[1][0] : (v[1][1] + 1)] = 1
    return np.where(allowed_arr == 1)[0]


def process_nearby_tickets(nearby_tickets_array):
    """
    return a matrix with 0/1 for numbers on the tickets
    """
    d2 = np.max(nearby_tickets_array) + 1
    out = np.zeros((nearby_tickets_array.shape[0], d2), dtype=np.int8)
    for i in range(out.shape[0]):
        out[i, nearby_tickets_array[i]] = 1
    assert (out.sum(axis=-1) <= nearby_tickets_array.shape[1]).all()
    return out


def check_possible_match(arr, v):
    return (
        ((arr >= v[0][0]) & (arr <= v[0][1])) | ((arr >= v[1][0]) & (arr <= v[1][1]))
    ).all()


def prune(arr):
    """
    make arr one-hot on the rows
    """
    while (arr.sum(axis=-1) > 1).any():
        # find all rows which have only one 1
        # good_rows = np.where(arr.sum(axis=-1) == 1)
        good_rows_bool = arr.sum(axis=-1) == 1
        good_rows = np.where(good_rows_bool)[0]
        # set the column where that 1 occurs to zero for all other columns
        for gr in good_rows:
            col = np.where(arr[gr] == 1)[0][0]
            arr[~good_rows_bool, col] = 0
    assert (arr.sum(axis=-1) == 1).all(), "prune fail"
    return arr


@click.command()
@click.argument("fn")
def main(fn):
    rule_dict, my_tick_array, nearby_ticks_array = read_data(fn)
    rule_array = process_rule_dict_to_array(rule_dict)
    nearby_ticket_bin_mat = process_nearby_tickets(nearby_ticks_array)
    # find invalid tickets and discard them
    valid_mask = (
        nearby_ticket_bin_mat[
            :,
            [x for x in range(nearby_ticket_bin_mat.shape[-1]) if x not in rule_array],
        ].sum(axis=-1)
        == 0
    )
    valid_nearby_tickets = nearby_ticks_array[valid_mask]
    # add your own ticket
    valid_tickets = np.concatenate(
        [my_tick_array[None, :], valid_nearby_tickets], axis=0
    )
    possible_arr = np.zeros((len(rule_dict), valid_tickets.shape[-1]), dtype=np.int8)
    for i, v in enumerate(rule_dict.values()):
        for j in range(valid_tickets.shape[-1]):
            possible_arr[i, j] = check_possible_match(valid_tickets[:, j], v)
    # prune possible_arr to solve the issue
    pruned_arr = prune(possible_arr)
    # get the column numbers for the departure entries
    departure_bools = np.array(["departure" in x for x in rule_dict.keys()])
    col_idxes = np.where(pruned_arr[departure_bools] == 1)[-1]
    answer = my_tick_array[col_idxes].prod()
    print("Answer = {}".format(answer))


if __name__ == "__main__":
    main()
