import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        line_nr = -1
        out = {}
        while line:
            line_nr += 1
            linesplit = line.replace("\n", "").split(" ")
            out[line_nr] = [linesplit[0], int(linesplit[1])]
            line = fp.readline()
    return out


def run_programme(dat):
    hit_counter = np.zeros(len(dat), dtype=np.int8)
    last_instruction_idx = len(dat) - 1
    instruction_idx = 0
    accumulator = 0
    keep_going = True
    while keep_going:
        # check whether we've been here before
        if hit_counter[instruction_idx] == 1:
            return np.nan  # we've hit the start of an inifinite loop
        else:
            # if not, follow instructions
            instruction, val = dat[instruction_idx]
            # but also update that we've been here
            hit_counter[instruction_idx] = 1
            if instruction == "nop":
                instruction_idx += 1
            elif instruction == "acc":
                accumulator += val
                instruction_idx += 1
            else:  # jump
                assert instruction == "jmp", "instruction error"
                instruction_idx += val
        # check whether we've reached the last line
        if instruction_idx == last_instruction_idx:
            # there's not infinite loop here, return the value of the accumulator
            return accumulator


@click.command()
@click.argument("fn")
def main(fn):
    orig_dat = read_data(fn)
    # brute force - change any jmp to nop and nop to jmp
    for i in range(len(orig_dat)):
        # undo previous change
        if i > 0:
            orig_dat[i - 1][0] = (
                orig_dat[i - 1][0]
                .replace("nop", "JMP")
                .replace("jmp", "nop")
                .replace("JMP", "jmp")
            )
        if (orig_dat[i][0] == "nop") or (orig_dat[i][0] == "jmp"):
            # change nop <-> jmp
            orig_dat[i][0] = (
                orig_dat[i][0]
                .replace("nop", "JMP")
                .replace("jmp", "nop")
                .replace("JMP", "jmp")
            )
            # run the new programme
            out = run_programme(orig_dat)
            if np.isfinite(out):
                print("value of accumulator for working programme = {}".format(out))
                print("changed instruction at {}".format(i))
                break
    print(i)


if __name__ == "__main__":
    main()