import numpy as np
import pandas as pd

# import itertools

import click

# import re
# import sys
# sys.setrecursionlimit(1500)


def read_data(fn):
    # return dict with rules and list of lines to check
    rule_dict = {}
    input_to_check = []
    mode = "rule"
    with open(fn, "r") as fp:
        line = fp.readline()
        while line:
            if mode == "rule":
                if line == "\n":
                    mode = "input"
                else:
                    k, vs = line.replace("\n", "").split(": ")
                    if "|" in vs:
                        rule_dict[k] = [v.split(" ") for v in vs.split(" | ")]
                    else:
                        # not the most elegant way, can probably just replace '"' by "" above
                        if vs == '"a"':
                            rule_dict[k] = ["a"]
                        elif vs == '"b"':
                            rule_dict[k] = ["b"]
                        else:
                            rule_dict[k] = [vs.split(" ")]
            else:  # mode = 'input'
                input_to_check += [line.replace("\n", "")]
            line = fp.readline()
    return rule_dict, input_to_check


def update_rule_dict(rule_dict):
    keys_resolved = [
        k
        for k in rule_dict.keys()
        if (rule_dict[k] == ["a"]) or (rule_dict[k] == ["b"])
    ]
    # print(keys_resolved)
    keys_unresolved = [x for x in rule_dict.keys() if x not in keys_resolved]
    # print(keys_unresolved)
    counter = 0
    while len(keys_unresolved) > 0:
        # go through unresolved keys, see whether anything in them is already resolved
        for k in keys_unresolved:
            rule = rule_dict[k].copy()  # [[a, b], [c], d]
            found_something = False
            for j, l1 in enumerate(rule):  # [a, b] or [c]  or d
                if type(l1) == list:
                    for i, l2 in enumerate(l1):  # (a or b) or (c)
                        if l2 in keys_resolved:
                            # print("Found {} in {} at {}".format(l2, k, (j, i)))
                            resolution = rule_dict[l2]
                            del rule[j]
                            rule += [
                                l1[:i] + ([x] if type(x) != list else x) + l1[i + 1 :]
                                for x in resolution
                            ]
                            # rule_dict[k] = rule
                            found_something = True
                            break
                if found_something:  # indentation??
                    rule_dict[k] = rule
                    # print("updated rule_dict[{}]".format(k))
                    # print("with {}".format(rule))
                    break
            # print("here")

            if found_something:
                # check for each sublist whether we can concatenate
                concatenated_something = False
                for j, l1 in enumerate(rule):
                    if "".join(l1).isalpha():
                        rule[j] = "".join(l1)
                        concatenated_something = True
                rule_dict[k] = rule

                if concatenated_something:
                    concatenated_all = True
                    # check whether all is concatenated now, ie, is the thing resolved
                    for j, l1 in enumerate(rule):
                        # print("checking {}".format(l1))
                        if type(l1) == list:
                            concatenated_all = False
                            break
                    if concatenated_all:
                        print("have resolved : {}".format(k))
                        keys_resolved += [k]
                        keys_unresolved.remove(k)
                        # print(keys_resolved)
                        assert k not in keys_unresolved
                        assert k in keys_resolved
        # print("11 resolved = {}".format("11" in keys_resolved))
        # print("8 resolved = {}".format("8" in keys_resolved))
        # print("0 resolved = {}".format("0" in keys_resolved))
        # print(len(rule_dict["0"]))
        # print(rule_dict["0"])
        # print(len(rule_dict["11"]))
        # print(rule_dict["11"])
        # print(len(rule_dict["8"]))
        # print("State of 11")
        # for i, r in enumerate(rule_dict["11"]):
        # if type(r) == list:
        # print(i, r)
        print("+++ round {} done +++".format(counter))
        counter += 1
        if ("8" in keys_resolved) & ("11" in keys_resolved):
            return rule_dict["8"], rule_dict["11"]
        # if counter == 200:
        #     return

    return rule_dict


def check(string, rule8, rule11):
    for r8 in rule8:
        if string[: len(r8)] == r8:
            for r11 in rule11:
                if string[len(r8) :] == r11:
                    return True
    return False


@click.command()
@click.argument("fn")
def main(fn):
    rule_dict, input_to_check = read_data(fn)
    rule8, rule11 = update_rule_dict(rule_dict)
    print("rule 8, 11 found")

    answer = np.array([check(l, rule8, rule11) for l in input_to_check])

    # print(rule_dict)
    # print(input_to_check)
    # answer = np.zeros(len(input_to_check), dtype=bool)
    # for i, l in enumerate(input_to_check):
    #     print("+++ {} +++".format(i))
    #     answer[i] = f(check_string(l, "0", rule_dict, None))
    # answer = np.array([M([l], ["0"], rule_dict) for l in input_to_check])  # .sum()
    # print("Answer = {}".format(answer))
    # for l, a in zip(input_to_check, answer):
    #     print(a, l)
    print("Answer = {}".format(answer.sum()))


if __name__ == "__main__":
    main()