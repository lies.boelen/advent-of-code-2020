import numpy as np
import pandas as pd

import click

import string


def main():
    start = np.array([12, 1, 16, 3, 11, 0], dtype=int)
    # start = np.array([1, 3, 2])
    game = np.ones(2020) * (-1)
    game[: len(start)] = start
    for k in range(len(start), 2020):
        prev_val = game[k - 1]
        count = np.sum(game == prev_val)
        if count == 1:
            game[k] = 0
        else:
            locs = np.where(game[: (k - 1)] == prev_val)[0]
            locs_max = np.max(locs)
            game[k] = (k - 1) - locs_max
    print(game[:20])
    print("2020th number in game = {}".format(game[-1]))


if __name__ == "__main__":
    main()
