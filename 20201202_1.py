import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as f:
        return f.read().split("\n")


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get np array
    counter_correct = 0
    for d in dat:
        if len(d) > 0:
            mm, letter, pw = d.split(" ")
            min_count, max_count = mm.split("-")
            min_count = int(min_count)
            max_count = int(max_count)
            letter = letter[0]
            pw_array = np.array(list(pw))
            letter_counter = (pw_array == letter).sum()
            if (letter_counter >= min_count) & (letter_counter <= max_count):
                counter_correct += 1
    print("Number of correct passwords = {}".format(counter_correct))


if __name__ == "__main__":
    prod = main()