import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as f:
        inp = f.read()
    inp = inp.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")
    inp = [list(x) for x in inp.split("\n")]
    rows = np.array([x[:-3] for x in inp]).astype(np.int8)
    seats = np.array([x[-3:] for x in inp]).astype(np.int8)
    return rows, seats


@click.command()
@click.argument("fn")
def main(fn):
    rows_bin, seats_bin = read_data(fn)
    rows = (rows_bin * np.power(2, np.arange(7)[::-1])[None, :]).sum(axis=-1)
    seats = (seats_bin * np.power(2, np.arange(3)[::-1])[None, :]).sum(axis=-1)
    seat_ids = rows * 8 + seats
    print("Highest seat ID = {}".format(np.max(seat_ids)))


if __name__ == "__main__":
    main()