import numpy as np
import pandas as pd

import itertools

import click

# import re
# import sys


PLAYER1 = [
    21,
    48,
    44,
    31,
    29,
    5,
    23,
    11,
    12,
    27,
    49,
    22,
    18,
    7,
    15,
    20,
    2,
    45,
    14,
    17,
    40,
    35,
    6,
    24,
    41,
]

PLAYER2 = [
    47,
    1,
    10,
    16,
    28,
    37,
    8,
    26,
    46,
    25,
    3,
    9,
    34,
    50,
    32,
    36,
    43,
    4,
    42,
    33,
    19,
    13,
    38,
    39,
    30,
]


def play_game(player1, player2):
    while len(player1) * len(player2) > 0:
        p1 = player1.pop(0)
        p2 = player2.pop(0)
        if p1 > p2:
            player1 += [p1, p2]
        else:
            player2 += [p2, p1]
    arr = player1 if len(player1) > 0 else player2
    return (np.array(arr) * (1 + np.arange(len(arr)))[::-1]).sum()


def main():
    answer = play_game(PLAYER1, PLAYER2)
    print("Answer = {}".format(answer))


if __name__ == "__main__":
    main()