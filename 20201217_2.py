import numpy as np
import pandas as pd
import itertools

import click

import string


def read_data(fn):
    # return 2D np array with 0/1 (np.int8)
    with open(fn, "r") as fp:
        input_list = []
        line = fp.readline().replace("\n", "")
        while line:
            input_list += [[np.int8(1) if x == "#" else np.int8(0) for x in line]]
            line = fp.readline().replace("\n", "")

    out = np.array(input_list)
    return out


def count_active_neighbours(game_array):
    counter = np.zeros_like(game_array)
    dx, dy, dz, dw = counter.shape
    opt_list = [-1, 0, 1]
    for a, b, c, d in itertools.product(opt_list, opt_list, opt_list, opt_list):
        # direction is an array of length 4 with entries 0/1/-1; 80 such directions
        # as we are not including (0, 0, 0, 0)
        if np.abs([a, b, c, d]).sum() > 0:
            counter[
                np.maximum(0, 0 + a) : np.minimum(dx, dx + a),
                np.maximum(0, 0 + b) : np.minimum(dy, dy + b),
                np.maximum(0, 0 + c) : np.minimum(dz, dz + c),
                np.maximum(0, 0 + d) : np.minimum(dw, dw + d),
            ] += game_array[
                np.maximum(0, 0 - a) : np.minimum(dx, dx - a),
                np.maximum(0, 0 - b) : np.minimum(dy, dy - b),
                np.maximum(0, 0 - c) : np.minimum(dz, dz - c),
                np.maximum(0, 0 - d) : np.minimum(dw, dw - d),
            ]
    return counter


def update_game_array(game_array):
    neighbour_count = count_active_neighbours(game_array)
    # update active cubes: exactly 2 or 3 active neighbours, otherwise become inactive
    cond_become_inactive = (game_array == 1) & (
        (neighbour_count < 2) | (neighbour_count > 3)
    )
    # update inactive cubes: have exactly 3 active neighbours
    cond_become_active = (game_array == 0) & (neighbour_count == 3)
    game_array[cond_become_inactive] = 0
    game_array[cond_become_active] = 1
    return game_array


@click.command()
@click.argument("fn")
@click.argument("rounds")
def main(fn, rounds):
    rounds = int(rounds)
    input_array = read_data(fn)
    dx, dy = input_array.shape
    # need to expand this to 4d
    expansion_size = 2 * rounds
    game_array = np.zeros(
        (
            dx + expansion_size,
            dy + expansion_size,
            1 + expansion_size,
            1 + expansion_size,
        ),
        dtype=np.int8,
    )
    game_array[rounds : rounds + dx, rounds : rounds + dy, rounds, rounds] = input_array
    # for six rounds, update this according to the game rules
    for game_round in range(rounds):
        game_array = update_game_array(game_array)
    print("Number of active cells = {}".format(game_array.sum()))


if __name__ == "__main__":
    main()
