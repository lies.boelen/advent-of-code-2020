import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    # return as numpy array
    return pd.read_csv(fn, header=None).values.flatten()


def check_sum(arr, el):
    """
    check whether el can be found as the sum of two values in arr
    """
    print("goal = {}".format(el))
    for i, el1 in enumerate(arr[:-1]):
        if el - el1 in arr[(i + 1) :]:
            return True
    return False


def find_first_wrong(arr, len_before=25):
    i = len_before
    while True:
        checksum = check_sum(arr[(i - len_before) : i], arr[i])
        if not checksum:
            print(arr[i], arr[(i - len_before) : i])
            return arr[i]
        i += 1


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    first_wrong = find_first_wrong(dat, len_before=25)
    print("first wrong value = {}".format(first_wrong))


if __name__ == "__main__":
    main()