import numpy as np
import pandas as pd

import click


def read_data(fn):
    return pd.Series(pd.read_csv(fn).iloc[:, 0]).values


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)  # get np array
    for i, d in enumerate(dat):
        complement = 2020 - d
        for j, e in enumerate(dat[i:]):
            complement_2 = complement - e
            if (complement_2 >= 0) & (complement_2 in dat[(j + 1) :]):
                prod = d * e * (2020 - d - e)
                print(
                    "{} + {} + {} = {}".format(d, e, complement_2, d + e + complement_2)
                )
                print(prod)
                return
    print(-1)


if __name__ == "__main__":
    prod = main()