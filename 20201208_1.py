import numpy as np
import pandas as pd

import click

import string


def read_data(fn):
    with open(fn, "r") as fp:
        # read into a dict
        line = fp.readline()
        line_nr = -1
        out = {}
        while line:
            line_nr += 1
            linesplit = line.replace("\n", "").split(" ")
            out[line_nr] = [linesplit[0], int(linesplit[1])]
            line = fp.readline()
    return out


@click.command()
@click.argument("fn")
def main(fn):
    dat = read_data(fn)
    hit_counter = np.zeros(len(dat), dtype=np.int8)
    instruction_idx = 0
    accumulator = 0
    keep_going = True
    while keep_going:
        # check whether we've been here before
        if hit_counter[instruction_idx] == 1:
            keep_going = False
        else:
            # if not, follow instructions
            instruction, val = dat[instruction_idx]
            # but also update that we've been here
            hit_counter[instruction_idx] = 1
            if instruction == "nop":
                instruction_idx += 1
            elif instruction == "acc":
                accumulator += val
                instruction_idx += 1
            else:  # jump
                assert instruction == "jmp", "instruction error"
                instruction_idx += val
    print("Value of accumulator before hitting infinite loop = {}".format(accumulator))


if __name__ == "__main__":
    main()