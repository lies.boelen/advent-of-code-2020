import numpy as np
import pandas as pd

import click


def read_data(fn):
    with open(fn, "r") as f:
        inp = f.read()
    inp = inp.replace("F", "0").replace("B", "1").replace("L", "0").replace("R", "1")
    inp = [list(x) for x in inp.split("\n")]
    rows = np.array([x[:-3] for x in inp]).astype(np.int8)
    seats = np.array([x[-3:] for x in inp]).astype(np.int8)
    return rows, seats


def get_seat_ids(rows_bin, seats_bin):
    rows = (rows_bin * np.power(2, np.arange(7)[::-1])[None, :]).sum(axis=-1)
    seats = (seats_bin * np.power(2, np.arange(3)[::-1])[None, :]).sum(axis=-1)
    seat_ids = rows * 8 + seats
    return seat_ids


def find_my_seat(seat_ids):
    all_seats = np.arange(127 * 8 + 7)
    unoccupied = np.array([x not in seat_ids for x in all_seats])
    before_occupied = np.array([False] + [x - 1 in seat_ids for x in all_seats[1:]])
    after_occupied = np.array([x + 1 in seat_ids for x in all_seats[:-1]] + [False])
    candidates = np.array(unoccupied & before_occupied & after_occupied)
    return np.where(candidates)[0]


@click.command()
@click.argument("fn")
def main(fn):
    rows_bin, seats_bin = read_data(fn)
    seat_ids_occupied = get_seat_ids(rows_bin, seats_bin)
    my_seat = find_my_seat(seat_ids_occupied)
    print("My seat ID = {}".format(my_seat))


if __name__ == "__main__":
    main()